from dataclasses import dataclass, asdict, field
import firebase_admin
from firebase_admin import credentials, firestore
from google.cloud.firestore import ArrayUnion, DocumentReference
from datetime import datetime
import tzlocal
import pytz
from enum import Enum
from . import utils


# Initialize Firebase
cred = credentials.Certificate(
    utils.get_sensible_value("fb-certificate-route"))
default_app = firebase_admin.initialize_app(cred)
db = firestore.client()

real_team_ref = db.collection(u'RealTeams')


@dataclass
class Model:
    id: str

    def add_to_fb(self):
        data = asdict(self)
        data.pop('id', None)
        collection_name = class_to_collection_name(self.__class__.__name__)
        db.collection(collection_name).document(self.id).set(data)

    def delete_fb(self):
        collection_name = class_to_collection_name(self.__class__.__name__)
        db.collection(collection_name).document(self.id).delete()


@dataclass
class RealTeam(Model):
    name: str
    longName: str
    players: list


@dataclass
class Player(Model):
    name: str
    totalPoints: int
    pointsLastMatch: int
    position: str
    value: float


@dataclass
class MatchDay(Model):
    matchDay: int = 0
    startMarket: datetime = None
    startMatchPeriod: datetime = None
    endDate: datetime = None
    matches: list = field(default_factory=lambda: [])

    def update_timezone(self):
        if self.startMarket is not None:
            self.startMarket = utils.change_timezone(self.startMarket)
        if self.startMatchPeriod is not None:
            self.startMatchPeriod = utils.change_timezone(
                self.startMatchPeriod)
        if self.endDate is not None:
            self.endDate = utils.change_timezone(self.endDate)


@dataclass
class Match(Model):
    homeTeamGoals: int
    awayTeamGoals: int
    homeTeam: DocumentReference
    awayTeam: DocumentReference


@dataclass
class UserTeam(Model):
    formation: str
    credit: float
    pointsLastMatch: int
    totalPoints: int
    userProfile: DocumentReference
    playersLineUp: list
    playersReserves: list


###############################################################################
#
# Enums
#
###############################################################################

def class_to_collection_name(class_name):
    switcher = {
        'RealTeam': u'RealTeams',
        'Player': u'Players',
        'MatchDay': u'MatchDays',
        'Match': u'Matches',
        'UserTeam': u'UserTeams'
    }
    return switcher.get(class_name, 'error')


def position_string_to_enum(position):
    switcher = {
        'Goalkeeper': 'GOALKEEPER',
        'Defender': 'DEFENDER',
        'Midfielder': 'MIDFIELDER',
        'Attacker': 'ATTACKER'
    }
    return switcher.get(position, 'DEFAULT')


###############################################################################
#
# Métodos Firestore
#
###############################################################################

# Añade lista de objetos y actualiza la lista de docRef del objeto padre
def batch_add(object_list, father_id, father_collection, father_array):
    collection_name = class_to_collection_name(
        object_list[0].__class__.__name__)
    batch = db.batch()
    father_list = []
    for e in object_list:
        data = asdict(e)
        data.pop('id', None)
        doc_ref = db.collection(collection_name).document(str(e.id))
        if father_id is not None:
            father_list.append(doc_ref)
        batch.set(doc_ref, data)
    if father_id is not None:
        father_ref = db.collection(father_collection).document(father_id)
        batch.update(father_ref, {father_array: father_list})
    batch.commit()


def get_all_collection(dataclass_name):
    data = []
    docs = db.collection(class_to_collection_name(dataclass_name)).stream()
    for doc in docs:
        d = doc.to_dict()
        d['id'] = doc.id
        klass = globals()[dataclass_name]
        data.append(klass(**d))
    return data


def get_document(dataclass_name, doc_id):
    doc = db.collection(class_to_collection_name(
        dataclass_name)).document(doc_id).get()
    d = doc.to_dict()
    d['id'] = doc.id
    klass = globals()[dataclass_name]
    return klass(**d)


def get_system_variable(doc_id):
    doc = db.collection(u'SystemVariables').document(doc_id).get()
    return doc.get('value')


def set_system_variable(doc_id,value):
    db.collection(u'SystemVariables').document(doc_id).update({u'value' : value})



def update_from_dict(data, collection_name):
    id = data['id']
    data.pop('id', None)
    db.collection(collection_name).document(id).update(data)


def docref_from_id(collection_name, id):
    return db.collection(collection_name).document(str(id))



###############################################################################
#
# Métodos Firestore personalizados
#
###############################################################################

def batch_update_players_userteams(players, user_teams):
    player_collection = class_to_collection_name(
        players[0].__class__.__name__)
    user_teams_collection = class_to_collection_name(user_teams[0].__class__.__name__)
    players_lists = [players[offs:offs+15] for offs in range(0, len(players), 15)]
    userteeams_lists = [user_teams[offs:offs+15] for offs in range(0, len(user_teams), 15)]
    for pl in players_lists:
        batch = db.batch()
        for p in pl:
            data = asdict(p)
            doc_ref = db.collection(player_collection).document(str(p.id))
            batch.update(doc_ref, {u'pointsLastMatch': data['pointsLastMatch'], u'credit': data['credit'], u'totalPoints': data['totalPoints']})
        batch.commit()
    for utl in userteeams_lists:
        for ut in utl:
            data = asdict(ut)
            doc_ref = db.collection(user_teams_collection).document(str(ut.id))
            batch.update(doc_ref, {u'pointsLastMatch': data['pointsLastMatch'], u'totalPoints': data['totalPoints']})
        batch.commit()