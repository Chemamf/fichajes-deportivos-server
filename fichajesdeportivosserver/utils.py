from datetime import datetime, date
from bs4 import BeautifulSoup
from . import sql
import tzlocal, pytz
from urllib.request import Request, urlopen


# Convierte String en Datetime con timezone local
def string_to_date(date_time_str):
    return datetime.strptime(date_time_str, '%Y/%m/%d %H:%M:%S').astimezone(pytz.UTC)


# Convierte fecha en string
def date_to_string(date_time_obj):
    return date_time_obj.strftime('%Y/%m/%d %H:%M:%S')


# Convierte string obtenido de formulario en Datetime con timezone local
def html_datetime_to_date(date_time_str):
    return datetime.strptime(date_time_str, '%Y-%m-%dT%H:%M').astimezone(pytz.UTC)


# Cambia el timezone de un Datetime, y actualiza su hora:
def change_timezone(datetime_obj):
    return datetime_obj.astimezone(pytz.timezone(tzlocal.get_localzone().zone))


def compare_datetimes(date1, date2):
    date1_obj = date(date1.year, date1.month, date1.day)
    date2_obj = date(date2.year, date2.month, date2.day)
    if date1_obj > date2_obj:
        return True
    else:
        return False


def get_sensible_value(variable):
    data = ' - '
    with open('./sensible_values.txt', 'r', encoding='utf-8') as input_file:
        lines = input_file.readlines()
        for line in lines:
            if variable in line:
                aux = line.split('=')
                data = str(aux[1]).strip()
                break
    return data


###############################################################################
#
# Web Scrapping
#
###############################################################################


def get_player_values_transfermarkt(id):
    url = sql.get_transfermarkt_url(id)
    f = urlopen(Request(url, headers={'User-Agent': 'Mozilla/5.0'}))
    soup = BeautifulSoup(f, 'html.parser')
    data = {}

    players = soup.find('table', {'class': 'items'}).find(
        'tbody').find_all('tr', {'class': ['odd', 'even']})
    for p in players:
        name = p.find('a', {'class': 'spielprofil_tooltip'}).text
        v = p.find('td', {'class': 'rechts'}).text
        s = v.split(' ')

        value_end = ''
        if s[1].startswith(u'mill'):
            value_end = '000000'
        elif s[1].startswith(u'mil'):
            value_end = '000'

        value_start = s[0]
        if u',' in value_start:
            value_start = value_start.replace(u',', '')
            if len(value_end) > 2:
                value_end = value_end[:-2]

        value = value_start + value_end
        data[name] = value
    return data
