from .. import sql, utils, controllers, models
from datetime import datetime
from . import teams_and_players as tp


def update_FB_points():
    teams = tp.teams_from_JSON()
    players = []
    for t in teams:
        players.extend(tp.players_from_JSON_and_DB(t.id))
    user_teams = models.get_all_collection(models.UserTeam.__name__)
    for i in range(len(user_teams)):
        pointsLM = 0
        for p in user_teams[i].playersLineUp:
            player = sql.get_value(p.id)
            if player['pointsLastMatch'] is not None:
                pointsLM += player['pointsLastMatch']
        user_teams[i].pointsLastMatch = pointsLM
        user_teams[i].credit += pointsLM * 200000
        user_teams[i].totalPoints = user_teams[i].totalPoints + pointsLM
    players_to_FB = []
    for p in players:
        if p.pointsLastMatch is not None:
            if p.totalPoints is not None:
                p.totalPoints = p.totalPoints + p.pointsLastMatch
            else:
                p.totalPoints = p.pointsLastMatch
            sql.edit_player_tpoints(
                    p.id, p.totalPoints)
            players_to_FB.append(p)
    models.batch_update_players_userteams(players_to_FB, user_teams)
    sql.edit_server_variable(
        u'last_update_points_fb', utils.date_to_string(datetime.now()))


def reset_all_lmpoints():
    teams = tp.teams_from_JSON()
    players = []
    for t in teams:
        players.extend(tp.players_from_JSON_and_DB(t.id))
    sql.reset_all_lmpoints(players)

def reset_all_tpoints():
    teams = tp.teams_from_JSON()
    players = []
    for t in teams:
        players.extend(tp.players_from_JSON_and_DB(t.id))
    sql.reset_all_tpoints(players)

def update_DB_player_points(player_id, new_points):
    sql.edit_player_lmpoints(player_id, new_points)
