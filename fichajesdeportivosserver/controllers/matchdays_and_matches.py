from .. import sql, utils, controllers, models
import http.client
import json, logging
from datetime import datetime

# football-data.org connection
api_connection = http.client.HTTPConnection('api.football-data.org')
headers = {'X-Auth-Token': utils.get_sensible_value("X-Auth-Token")}


###############################################################################
#
# Jornadas
#
###############################################################################

# Obtiene solo número de la jornada desde JSON
def get_current_matchday_from_json():
    with open('./json/leagues/LaLigaRealTeams.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
        current = int(data['season'].get('currentMatchday'))
    return current


def get_current_matchday_firebase():
    return models.get_system_variable(u'currentMatchDay')


def get_all_matchdays_firebase():
    matchdays = models.get_all_collection(models.MatchDay.__name__)
    for m in matchdays:
        m.update_timezone()
    return matchdays


def filter_actual_matchdays(current, matchdays):
    data = []
    for md in matchdays:
        if md.matchDay == current or md.matchDay == current-1:
            data.append(md)
        if len(data) >= 2:
            break
    return data


def dates_to_firebase(id, startMarket, startMatchPeriod, endDate):
    data = {u'id': id}
    if startMarket:
        data[u'startMarket'] = utils.html_datetime_to_date(startMarket)
    if startMatchPeriod:
        data[u'startMatchPeriod'] = utils.html_datetime_to_date(
            startMatchPeriod)
    if endDate:
        data[u'endDate'] = utils.html_datetime_to_date(endDate)
    models.update_from_dict(data, 'MatchDays')


def update_matchday():
    current = get_current_matchday_from_json()
    # Eliminada funcion para actualizar la jornada según API, se hará segun fecha de finalización de jornada
    #models.set_system_variable(u'currentMatchDay', current)


def add_default_matchdays(matchdays_number):
    matchdays = []
    for i in range(1, matchdays_number):
        matchdays.append(models.MatchDay(id=i, matchDay=i))
        print(i)
    models.batch_add(matchdays, u'2014', u'Leagues', u'matchDays')


###############################################################################
#
# Partidos
#
###############################################################################

def matches_from_api_to_JSON(id):
    api_connection.request(
        'GET', u'/v2/competitions/2014/matches?matchday=' + str(id), None, headers)
    response = json.loads(api_connection.getresponse().read().decode())
    with open('./json/matches/Matches_Matchday_' + str(id) + '.json', 'w+', encoding='utf-8') as f:
        json.dump(response, f, ensure_ascii=False, indent=4)
    sql.edit_server_variable(
        u'last_update_get_matchday_' + str(id) + '_matches_api', utils.date_to_string(datetime.now()))


def get_teams_dict():
    teams = {}
    with open('./json/leagues/LaLigaRealTeams.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
        for j_team in data['teams']:
            teams[str(j_team.get(u'id'))] = j_team.get(u'name')
    return teams


def get_matches_from_JSON(matchday_id):
    matches = []
    try:
        with open('./json/matches/Matches_Matchday_' + matchday_id + '.json', 'r+', encoding='utf-8') as f:
            data = json.load(f)
            for j_match in data['matches']:
                if j_match.get(u'status') == u'FINISHED':
                    matches.append(models.Match(j_match.get(u'id'),
                                                j_match.get(u'score').get(
                                                    u'fullTime').get(u'homeTeam'),
                                                j_match.get(u'score').get(
                                                    u'fullTime').get(u'awayTeam'),
                                                models.docref_from_id(
                        u'RealTeams', j_match.get(u'homeTeam').get(u'id')),
                        models.docref_from_id(u'RealTeams', j_match.get(u'awayTeam').get(u'id'))))
                else:
                    matches.append(models.Match(j_match.get(u'id'),
                                                None,
                                                None,
                                                models.docref_from_id(
                                                    u'RealTeams', j_match.get(u'homeTeam').get(u'id')),
                                                models.docref_from_id(u'RealTeams', j_match.get(u'awayTeam').get(u'id'))))
    except IOError:
        logging.exception('File not exists')
    return matches


def matches_from_JSON_to_FB(matchday_id):
    matches = get_matches_from_JSON(matchday_id)
    models.batch_add(matches, matchday_id, u'MatchDays', u'matches')
