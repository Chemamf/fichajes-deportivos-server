from .. import sql, utils, controllers, models
import http.client
import json
from datetime import datetime

# football-data.org connection
api_connection = http.client.HTTPConnection('api.football-data.org')
headers = {'X-Auth-Token': utils.get_sensible_value("X-Auth-Token")}


def get_variables():
    return sql.get_all_server_variables()


###############################################################################
#
# Equipos
#
###############################################################################

def teams_from_API_to_JSON(id):
    api_connection.request('GET', '/v2/competitions/' +
                           str(id) + '/teams', None, headers)
    response = json.loads(api_connection.getresponse().read().decode())
    with open('./json/leagues/LaLigaRealTeams.json', 'w+', encoding='utf-8') as f:
        json.dump(response, f, ensure_ascii=False, indent=4)
    sql.edit_server_variable(
        u'last_update_get_teams_api', utils.date_to_string(datetime.now()))


def teams_from_JSON():
    teams = []
    with open('./json/leagues/LaLigaRealTeams.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
        for j_team in data['teams']:
            team = models.RealTeam(j_team.get(u'id'), j_team.get(
                u'shortName'), j_team.get(u'name'), [])
            teams.append(team)
    return teams


def teams_from_JSON_to_FB():
    teams = teams_from_JSON()
    models.batch_add(teams, '2014', 'Leagues', 'realTeams')
    sql.edit_server_variable(
        u'last_update_set_teams_fb', utils.date_to_string(datetime.now()))


###############################################################################
#
# Jugadores
#
###############################################################################

def players_from_API_to_JSON(id):
    api_connection.request('GET', '/v2/teams/' + str(id), None, headers)
    response = json.loads(api_connection.getresponse().read().decode())
    with open('./json/teams/Team_' + str(id) + '.json', 'w+', encoding='utf-8') as f:
        json.dump(response, f, ensure_ascii=False, indent=4)
    sql.edit_server_variable(
        u'last_update_get_team_' + str(id) + '_players_api', utils.date_to_string(datetime.now()))


def players_from_TM_to_BD(id):
    data = {}
    api_players = players_from_JSON_and_DB(id)
    players = utils.get_player_values_transfermarkt(id)
    for p in players:
        for t in api_players:
            if p == t.name:
                data[t.id] = players[p]
                break
    sql.edit_multiple_player_values(data)
    sql.edit_server_variable(
        u'last_update_get_team_' + str(id) + '_values_tm', utils.date_to_string(datetime.now()))


# Devuelve un equipo, sin jugadores
def get_one_team_from_JSON(id):
    with open('./json/leagues/LaLigaRealTeams.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
        for j_team in data['teams']:
            if j_team.get(u'id') == int(id):
                return models.RealTeam(j_team.get(u'id'), j_team.get(
                    u'shortName'), j_team.get(u'name'), [])
    return None


# Devuelve lista de jugadores de un equipo
def players_from_JSON_and_DB(team_id):
    players = []
    with open('./json/teams/Team_' + str(team_id) + '.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
        for j_player in data['squad']:
            if j_player.get(u'role') == 'PLAYER':
                sql_player = sql.get_value(j_player.get(u'id'))
                player = models.Player(j_player.get(u'id'), j_player.get(
                    u'name'), sql_player.get('totalPoints', 0), sql_player.get(
                        'pointsLastMatch', 0), models.position_string_to_enum(j_player.get(u'position')), sql_player.get('value', 0))
                players.append(player)
    return players


def edit_bd_player_value(id, new_value):
    sql.edit_player_value(id, new_value)


def players_from_JSON_and_BD_to_FB(team_id):
    players = players_from_JSON_and_DB(team_id)
    models.batch_add(players, str(team_id), 'RealTeams', 'players')
    sql.edit_server_variable(
        u'last_update_set_team_' + str(team_id) + '_players_fb', utils.date_to_string(datetime.now()))
