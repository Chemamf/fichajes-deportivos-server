import sqlite3

###############################################################################
#
# Server variables
#
###############################################################################


def edit_server_variable(variable, new_value):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    db_cursor.execute('''INSERT OR REPLACE INTO server_variables (variable, value)
        VALUES (?,?)''', (variable, new_value))

    db_connection.commit()
    db_connection.close()


def get_server_variable(variable):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    db_cursor.execute(
        '''SELECT value FROM server_variables WHERE variable = ?''', (variable,))
    data = db_cursor.fetchone()[0]

    db_connection.commit()
    db_connection.close()

    return data


def get_all_server_variables():
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    db_cursor.execute(
        '''SELECT * FROM server_variables'''
    )
    rows = db_cursor.fetchall()
    data = {}
    for row in rows:
        data[row[0]] = row[1]

    db_connection.commit()
    db_connection.close()

    return data


###############################################################################
#
# SQL - player values
#
###############################################################################

# Obtiene valor de un jugador a través de su id
def get_value(id):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    db_cursor.execute(
        '''SELECT * FROM player_values WHERE id = ?''', (id,))
    fetch = db_cursor.fetchone()
    data = {}
    if fetch is not None:
        data['value'] = fetch[1]
        data['pointsLastMatch'] = fetch[2]
        data['totalPoints'] = fetch[3]

    db_connection.commit()
    db_connection.close()

    return data


def get_transfermarkt_url(id):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    db_cursor.execute(
        '''SELECT url FROM transfermarkt_teams WHERE id = ?''',  (id,))
    data = db_cursor.fetchone()[0]

    db_connection.commit()
    db_connection.close()

    return data


def edit_player_value(id, value):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    db_cursor.execute(
        '''UPDATE player_values SET value = ? WHERE id = ?''', (value, id))

    db_connection.commit()
    db_connection.close()


# Recibe diccionario con clave/valor id jugador/valor jugador
def edit_multiple_player_values(values):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    query = u'INSERT OR REPLACE INTO player_values (id, value) VALUES '
    for v in values:
        query += u'(\"' + str(v) + u'\", \"' + str(values[v]) + u'\"), '
    query = query[:-2]
    query += u';'
    db_cursor.execute(query)

    db_connection.commit()
    db_connection.close()


###############################################################################
#
# SQL - player points
#
###############################################################################

def edit_player_lmpoints(id, points):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    db_cursor.execute('''UPDATE player_values SET pointsLastMatch = ? WHERE id = ?''', (points, id))

    db_connection.commit()
    db_connection.close()


def edit_player_tpoints(id, points):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    db_cursor.execute('''UPDATE player_values SET totalPoints = ? WHERE id = ?''', (points, id))

    db_connection.commit()
    db_connection.close()


def reset_all_lmpoints(players):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    for p in players:
        db_cursor.execute('''UPDATE player_values SET pointsLastMatch = ? WHERE id = ?''', (0, p.id))

    db_connection.commit()
    db_connection.close()

def reset_all_tpoints(players):
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    for p in players:
        db_cursor.execute('''UPDATE player_values SET totalPoints = ? WHERE id = ?''', (0, p.id))

    db_connection.commit()
    db_connection.close()


###############################################################################
#
# SQL - create tables
#
###############################################################################


def create_tables_bd():
    db_connection = sqlite3.connect('fichajes_deportivos.db')
    db_cursor = db_connection.cursor()

    db_cursor.execute('''CREATE TABLE IF NOT EXISTS server_variables (
        variable text PRIMARY KEY,
        value text);''')

    db_cursor.execute('''CREATE TABLE IF NOT EXISTS transfermarkt_teams(
        id text PRIMARY KEY,
        url text);''')

    db_cursor.execute(
        '''INSERT OR REPLACE INTO transfermarkt_teams (id,url) VALUES ("77","https://www.transfermarkt.es/athletic-bilbao/startseite/verein/621/saison_id/2020"),
        ("78", "https://www.transfermarkt.es/atletico-madrid/startseite/verein/13/saison_id/2020"),
        ("79", "https://www.transfermarkt.es/ca-osasuna/startseite/verein/331/saison_id/2020"),
        ("81", "https://www.transfermarkt.es/fc-barcelona/startseite/verein/131/saison_id/2020"),
        ("82", "https://www.transfermarkt.es/fc-getafe/startseite/verein/3709/saison_id/2020"),
        ("83", "https://www.transfermarkt.es/fc-granada/startseite/verein/16795/saison_id/2020"),
        ("86", "https://www.transfermarkt.es/real-madrid/startseite/verein/418/saison_id/2020"),
        ("88", "https://www.transfermarkt.es/ud-levante/startseite/verein/3368/saison_id/2020"),
        ("90", "https://www.transfermarkt.es/real-betis-sevilla/startseite/verein/150/saison_id/2020"),
        ("92", "https://www.transfermarkt.es/real-sociedad-san-sebastian/startseite/verein/681/saison_id/2020"),
        ("94", "https://www.transfermarkt.es/fc-villarreal/startseite/verein/1050/saison_id/2020"),
        ("95", "https://www.transfermarkt.es/fc-valencia/startseite/verein/1049/saison_id/2020"),
        ("250", "https://www.transfermarkt.es/real-valladolid/startseite/verein/366/saison_id/2020"),
        ("263", "https://www.transfermarkt.es/deportivo-alaves/startseite/verein/1108/saison_id/2020"),
        ("264", "https://www.transfermarkt.es/cadiz-cf/startseite/verein/2687/saison_id/2020"),
        ("278", "https://www.transfermarkt.es/sd-eibar/startseite/verein/1533/saison_id/2020"),
        ("285", "https://www.transfermarkt.es/fc-elche/startseite/verein/1531/saison_id/2020"),
        ("299", "https://www.transfermarkt.es/sd-huesca/startseite/verein/5358/saison_id/2020"),
        ("558", "https://www.transfermarkt.es/celta-vigo/startseite/verein/940/saison_id/2020"),
        ("559", "https://www.transfermarkt.es/fc-sevilla/startseite/verein/368/saison_id/2020");''')

    db_cursor.execute('''CREATE TABLE IF NOT EXISTS player_values(
        id text PRIMARY KEY,
        value int,
        pointsLastMatch INTEGER,
        totalPoints INTEGER);''')

    db_connection.commit()
    db_connection.close()
