from flask import Flask, render_template, request

from fichajesdeportivosserver.controllers import (teams_and_players as tp,
                                                  matchdays_and_matches as mm,
                                                  points as pts)

import logging
import operator


# Initialize Flask app
app = Flask(__name__)
app.config.from_object('config')


@app.route('/')
def home():
    return render_template('pages/home.html')


@app.route('/teams', methods=['GET', 'POST'])
def teams():
    if request.method == 'POST':
        if 'get_teams_API' in request.form:
            tp.teams_from_API_to_JSON(2014)
        elif 'set_teams_FB' in request.form:
            tp.teams_from_JSON_to_FB()
        elif 'get_players_api' in request.form:
            tp.players_from_API_to_JSON(request.form['id'])
        elif 'get_players_TM' in request.form:
            tp.players_from_TM_to_BD(request.form['id'])
        elif 'set_players_FB' in request.form:
            logging.info("Disabled")
            tp.players_from_JSON_and_BD_to_FB(request.form['id'])
    teams = tp.teams_from_JSON()
    values = tp.get_variables()
    return render_template('pages/teams.html', teams=teams, values=values)


@app.route('/players/<team_id>', methods=['GET', 'POST'])
def players(team_id):
    if request.method == 'POST':
        if 'edit_player_value' in request.form:
            tp.edit_bd_player_value(
                request.form['id'], request.form['new_value'])
    team = tp.get_one_team_from_JSON(team_id)
    players = tp.players_from_JSON_and_DB(team_id)
    players.sort(key=operator.attrgetter('name'))
    return render_template('pages/players.html', team=team, players=players)


@app.route('/matchdays', methods=['GET', 'POST'])
def matchdays():
    if request.method == 'POST':
        if 'update' in request.form:
            tp.teams_from_API_to_JSON(2014)
            mm.update_matchday()
        if 'edit_dates' in request.form:
            mm.dates_to_firebase(request.form['id'], request.form['new_startMarket'],
                                 request.form['new_startMatchPeriod'], request.form['new_endDate'])
    current_matchday_JSON = mm.get_current_matchday_from_json()
    current_matchday_FB = mm.get_current_matchday_firebase()
    fb_matchdays = mm.get_all_matchdays_firebase()
    fb_matchdays = sorted(fb_matchdays, key=lambda x: x.matchDay, reverse=True)
    current_matchdays = mm.filter_actual_matchdays(
        current_matchday_JSON, fb_matchdays)
    values = tp.get_variables()
    return render_template('pages/matchdays.html', current_matchday_JSON=current_matchday_JSON, current_matchday_FB=current_matchday_FB,
                           values=values, fb_matchdays=fb_matchdays, current_matchdays=current_matchdays)


@app.route('/matches/<matchday_id>', methods=['GET', 'POST'])
def matches(matchday_id):
    if request.method == 'POST':
        if 'update_JSON' in request.form:
            mm.matches_from_api_to_JSON(matchday_id)
        if 'update_FB' in request.form:
            mm.matches_from_JSON_to_FB(matchday_id)
    values = tp.get_variables()
    teams = mm.get_teams_dict()
    matches = mm.get_matches_from_JSON(matchday_id)
    return render_template('pages/matches.html', values=values, matchday_id=matchday_id, teams=teams, matches=matches)


@app.route('/points', methods=['GET', 'POST'])
def points():
    if request.method == 'POST':
        if 'update_points_FB' in request.form:
            pts.update_FB_points()
        elif 'reset_all_pointsLM' in request.form:
            pts.reset_all_lmpoints()
    teams = tp.teams_from_JSON()
    teams1 = teams[:len(teams)//2]
    teams2 = teams[len(teams)//2:]
    values = tp.get_variables()
    return render_template('pages/points.html', teams1=teams1, teams2=teams2, values=values)


@app.route('/team_points/<team_id>', methods=['GET', 'POST'])
def team_points(team_id):
    if request.method == 'POST':
        if 'edit_player_points' in request.form:
            pts.update_DB_player_points(request.form['id'], request.form['new_points'])
    players = tp.players_from_JSON_and_DB(team_id)
    players.sort(key=operator.attrgetter('name'))
    team = tp.get_one_team_from_JSON(team_id)
    return render_template('pages/team_points.html', players=players, team=team)
